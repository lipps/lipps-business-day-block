<?php

/**
 * Plugin Name: Lipps BusinessDay Block
 * Description: Custom block for Lipps
 * Version: 1.0.1
 * Author: Neulab Inc.
 *
 * @package lipps-business-day-block
 */

defined( 'ABSPATH' ) || exit;

function lipps_business_day_render( $attributes ) {
	$post_id = array_key_exists( 'post_id', $attributes ) ? $attributes['post_id'] : '';

	if ( get_post_type($post_id) !== 'staff' ) {
		return '<p>この投稿タイプでは表示できません</p>';
	}

	$custom_field = get_post_meta( $post_id );

	$fixed_holidays = array_key_exists( 'fixed_holidays', $custom_field) !== false ? unserialize($custom_field['fixed_holidays'][0]) : '';
	$holidays = array_key_exists( 'holidays', $custom_field) !== false ? unserialize($custom_field['holidays'][0]) : '';

	if ( empty( $fixed_holidays ) ) {
		$fixed_holidays = array('');
	}
	if ( empty( $holidays ) ) {
		$holidays = array('');
	}

	return sprintf('
        <div class="wp-block-lipps-lipps-business-day-block">
           <input type="radio" name="business-day" id="calendar-this-month" checked> 
           <input type="radio" name="business-day" id="calendar-next-month">
           <div class="business-day-container">           
             <section id="calendar1">%1$s</section>
             <section id="calendar2">%2$s</section>
           </div>
           <p class="arrow previous">
             <span class="calendar-icon"></span>
               <label for="calendar-this-month"></label>
               <label for="calendar-next-month"></label>
           </p>
           <p class="arrow next">
             <span class="calendar-icon"></span>
               <label for="calendar-this-month"></label>
               <label for="calendar-next-month"></label>
           </p>
         </div>'
		, holiday_calendar(this_year(), this_month(), $holidays, $fixed_holidays)
		, holiday_calendar(year_of_the_next_month(), next_month(), $holidays, $fixed_holidays)
	);
// 2020/08
}

function lipps_business_day_register_block() {

	if ( ! function_exists( 'register_block_type' ) ) {
		// Gutenberg is not active.
		return;
	}

	date_default_timezone_set('Asia/Tokyo');

	wp_register_script(
		'lipps-business-day-block',
		plugins_url( 'build/index.js', __FILE__ ),
		array( 'wp-blocks', 'wp-element', 'wp-components'),
		filemtime( plugin_dir_path( __FILE__ ) . 'build/index.js' )
	);

	wp_register_style(
		'lipps-business-day-block-editor',
		plugins_url( 'style.css', __FILE__ ),
		array( 'wp-edit-blocks' ),
		filemtime( plugin_dir_path( __FILE__ ) . 'style.css' )
	);

	register_block_type( 'lipps/lipps-business-day-block', array(
		'attributes' => array(
			'post_id' => array (
				'type' => 'integer'
			),
		),
		'style'    => 'lipps-business-day-block-editor',
		'editor_style'    => 'lipps-business-day-block-editor',
		'editor_script'   => 'lipps-business-day-block',
		'render_callback' => 'lipps_business_day_render'
	) );
}
add_action( 'init', 'lipps_business_day_register_block' );

/*
* カレンダーを書くためのユーティリティ関数群
*/
function this_year() {
	$year = (int)date('Y' );
	return $year;
}

function this_month() {
	$month = (int)date('m' );
	return $month;
}

function next_month() {
	$month = (int)date('m' );
	$month = $month + 1 > 12 ? 1 : $month + 1;
	return $month;
}

function year_of_the_next_month() {
	$year =(int)date('Y' );
	$month = (int)date('m' );
	$year = $month === 12 ? $year + 1 : $year;
	return $year;
}

function first_weekday_of_the_month($year, $month) {
	$weekday = (int)date('w', mktime(0, 0, 0, $month, 1, $year));
	return $weekday;
}

function last_day_of_the_month($year, $month) {
	$last_day = (int)date('j', mktime(0, 0, 0, $month + 1, 0, $year));
	return $last_day;
}

function date_string($year, $month, $day) {
	return date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
}

function isHoliday($holidays, $today, $iWeekDay, $fixed_holidays) {
	if ( in_array( (string)$iWeekDay, $fixed_holidays, true) ) {
		return true;
	}

	if ( in_array($today, $holidays, true) ) {
		return true;
	}
	return false;
}

/*
* カレンダーの表示関数
*/
function holiday_calendar($year, $month, $field, $fixed_holidays) {
	$cal = '<table class="monthly-calendar">';
	$cal .= '<caption class="calendar-caption top">'.$year.'年'.$month.'月'.'</caption>';
	$cal .= '<thead><tr>';
	$cal .= '<th>日</th> <th>月</th> <th>火</th> <th>水</th> <th>木</th> <th>金</th> <th>土</th>';
	$cal .= '</tr></thead>';
	$cal .= '<tbody>';

	$iDay = 1;
	$OutOfMonth = false;
	while ( !$OutOfMonth ) {
		$cal .= '<tr>';
		for ( $iWeekDay=0; $iWeekDay<7; $iWeekDay++ ) {
			$today = date_string($year, $month, $iDay);
			$cal .= isHoliday($field, $today, $iWeekDay, $fixed_holidays) ? '<td class="holiday">' : '<td>';
			if ( $iDay===1 && $iWeekDay < first_weekday_of_the_month( $year,$month ) ) {
				// 1日が始まる曜日まではスキップ
				$cal .= '</td>';
				continue;
			} elseif( $OutOfMonth ) {
				// 月の最終日を以降はスキップ
				$cal .= '</td>';
			} else {
				$cal .= $iDay;
				$cal .= '<br></td>';
			}
			$iDay++;
			if ( !( $iDay < last_day_of_the_month($year, $month) + 1) ) {
				$OutOfMonth = true;
			}
		}
		$cal .= '</tr>';
	}
	$cal .= '</tbody>';
	//$cal .= '<caption class="calendar-caption bottom">※灰色の日はお休みです</caption>';
	$cal .= '</table>';
	return $cal;
}

